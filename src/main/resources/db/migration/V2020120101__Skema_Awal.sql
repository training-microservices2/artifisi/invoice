create table invoice (
    id varchar(36),
    sales_reference varchar(36) not null,
    transaction_time timestamp not null,
    customer_name varchar(100) not null,
    customer_email varchar(100) not null,
    description varchar(255) not null,
    amount decimal(19,2) not null,
    primary key (id)
)